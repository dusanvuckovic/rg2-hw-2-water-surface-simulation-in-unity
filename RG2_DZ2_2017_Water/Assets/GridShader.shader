﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/GridShader" {
	Properties	{		
		_MainTex("Main Texture", 2D) = "white" {}
		_Color("Water Color", Color) = (1, 1, 1, 1)
		_WaterHeight("Water Height", Range(1.0, 100.0)) = 10
		_SkyFactor("Sky factor", Range(0, 1)) = 0.2
		_CellSize("Cell size", Float) = 1
	}
	SubShader {				
		//Tags{ "DisableBatching" = "true" }
		Pass {
			Name "DrawWater"

			CGPROGRAM
			// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
			//#pragma exclude_renderers d3d11 gles
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			//vertex shader inputs
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 uv : TEXCOORD0;				
			};

			//vertex shader outputs ("vertex to fragment")
			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;				
				float3 lightDir : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
			};

			sampler2D _MainTex;
			float4 _Color;
			float _CellSize;
			float _WaterHeight;	
			float _SkyFactor;	
			int xPressed;
			int yPressed;

			//vertex shader
			v2f vert(appdata v) {
				v2f o;
			
				float4 vals = tex2Dlod(_MainTex, float4(v.uv, 0, 0));				

				//find the realistic normal, the passed normal is bad
				/*float diff = 1 / 256.0f;
				float x = v.uv.x;
				float y = v.uv.y;

				float coordLeft = max(0, x - diff);
				float coordUp = min(1, y + diff);

				float2 left = float2(coordLeft, y);
				float2 up = float2(x, coordUp);

				float4 valsLeft = tex2Dlod(_MainTex, float4(left, 0, 0));
				float4 valsUp = tex2Dlod(_MainTex, float4(up, 0, 0));

				float3 vec1 = float3(1, vals.r - valsLeft.r, 1);
				float3 vec2 = float3(1, valsUp.r - vals.r, 1);
				float3 normal = normalize(cross(vec1, vec2));	*/
				

				
				int p = (((v.vertex.x - xPressed) < 0.1) && ((v.vertex.z - yPressed) < 0.1)) ? 1 : 0;

				v.vertex.y += (vals.r + p)*_WaterHeight;

				/*float4 vertexLeft = float4(v.vertex.x, v.vertex.y + valsLeft.r*_WaterHeight, v.vertex.z, 1);
				float4 vertexUp = float4(v.vertex.x, v.vertex.y + valsUp.r*_WaterHeight, v.vertex.z, 1);*/

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;				
				/*float3 worldPosLeft = mul(unity_ObjectToWorld, v.vertexLeft).xyz;
				float3 worldPosRight = mul(unity_ObjectToWorld, v.vertexUp).xyz;*/

				o.vertex = UnityObjectToClipPos(v.vertex);				
				o.normal = v.normal;				
				o.lightDir = normalize(UnityWorldSpaceLightDir(worldPos));
				o.viewDir = normalize(_WorldSpaceCameraPos.xyz - worldPos.xyz);
				o.uv = v.uv;	

				return o;
			}

			float4 frag(v2f i) : SV_Target {
				float4 vals = tex2D(_MainTex, i.uv);
				float3 normal = normalize(i.normal);
				float3 lightDir = normalize(i.lightDir);
				float lambert = max(0, dot(normal, lightDir));
				float3 reflection = reflect(-lightDir, normal);
				float specular = pow(max(0, dot(normalize(i.viewDir), reflection)), 150);
				float alpha = 1 - dot(i.viewDir, normal);
				float4 skyData = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, reflection);
				float3 skyColor = DecodeHDR(skyData, unity_SpecCube0_HDR);

				
				//return float4(normal, 1); //write normals in world space
				return float4(_Color.rgb * lambert * (1-_SkyFactor) + _SkyFactor*skyColor + float3(specular, specular, specular), alpha);
			}
			ENDCG
		}
	}
	
}

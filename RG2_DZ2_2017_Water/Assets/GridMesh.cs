﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class GridMesh : MonoBehaviour { 

    private int gridSize = 256;
    public float cellSize = 0.1f;
    private Shader textureShader;
    private MeshCollider meshCollider;
    public CustomRenderTexture renderTexture;
    
    Mesh mesh;
    Vector3[] vertices;
    int[] triangles;
    private int xPressed, yPressed;
    Vector2[] uv;

    private void Awake() {
        gridSize = 256;        
        renderTexture.Initialize();
        
    }    

    // Use this for initialization
    void Start()
    {
        textureShader = Shader.Find("Unlit/RTexUpdateShader");
        Shader.SetGlobalFloat("input", 0);
        mesh = GetComponent<MeshFilter>().mesh;
        MakeMeshData();
        GenerateMesh();        
        meshCollider = GetComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh;
        //meshCollider.material = GetComponent<Material>();
    }

    private void MakeMeshData()
    {
        vertices = new Vector3[(gridSize + 1) * (gridSize + 1)];
        triangles = new int[gridSize * gridSize * 6];

        for (int i = 0, x = 0; x <= gridSize; x++) {
            for (int y = 0; y <= gridSize; y++, i++) {
                vertices[i] = new Vector3(y, 0, x);
            }
        }

        for (int ti = 0, vi = 0, x = 0; x < gridSize; x++, vi++) {
            for (int y = 0; y < gridSize; y++, ti += 6, vi++) {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + gridSize + 1;
                triangles[ti + 5] = vi + gridSize + 2;
            }
        }

        uv = new Vector2[vertices.Length];
        for (int i = 0, x = 0; x <= gridSize; x++)
            for (int y = 0; y <= gridSize; y++, i++) 
                uv[i] = new Vector2((float)x/gridSize, (float)y/gridSize);                                 

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
    }

    private void GenerateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }

    /*void OnMouseOver()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
        Debug.Log("Mouse is over GameObject.");
    }

    void OnMouseExit()
    {
        //The mouse is no longer hovering over the GameObject so output this message each frame
        Debug.Log("Mouse is no longer on GameObject.");
    }*/

    // Update is called once per frame
    void Update()
    {

        xPressed = yPressed = 0;
        if (Input.GetMouseButton(0)) {
            Vector3 screenMousePos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //print("origin: " + ray.origin);
            //print("direction: " + ray.direction);
            RaycastHit hit;
            //print(screenMousePos);
            if (Physics.Raycast(ray, out hit)) {
                double x = System.Math.Floor(hit.point.x),
                       y = System.Math.Floor(hit.point.z);
                xPressed = (int)x;
                yPressed = (int)y;


                int val = (int)(x * gridSize + y);
                /*Debug.Log("Mouse Down hit the following object: " + hit.collider.name);
                Debug.Log("at " + x + " and " + y);
                Debug.Log("which equals pos: " + val);*/
            }
            /*else
                Debug.Log("Nothing was hit!");*/         
         
        }
        Shader.SetGlobalInt("xPressed", xPressed);
        Shader.SetGlobalInt("yPressed", yPressed);        
    }


    /*   
       private Texture2D MakeGrayscale(Texture2D texture) {
        Texture2D textureN = Instantiate(texture);
        Color[] colors = textureN.GetPixels();
        for (int i = 0; i < colors.Length; i++) {
            var grayValue = colors[i].grayscale;
            colors[i] = new Color(grayValue, grayValue, grayValue, colors[i].a);
        }
        textureN.SetPixels(colors);
        textureN.Apply();
        return textureN;
    }
    */
    /*
    private void InitializeTexture(Texture2D texture) {
        Color[] colors = texture.GetPixels();
        for (int i = 0; i < colors.Length; i++) {
            colors[i] = new Color(0.5f, 0, 1f, 1);
        }
        texture.SetPixels(colors);
        texture.Apply();
    } 
    */

    /*     
    private void SendTextureToShader() {
        Texture2D myGUITexture = new Texture2D(gridSize, gridSize, textureFormat, false);
        InitializeTexture(myGUITexture);
    }    
    */

    /*
    private void IncrementTexture() {
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                vertices[i * gridSize + j].y += Random.Range(-0.01f, 0.01f);
    }
    */

    /*
    void DoPhysics() {
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                int n = i * gridSize + j;
                int a = (i == 0) ? i * gridSize + j : (i - 1) * gridSize + j;
                int b = (i == gridSize - 1) ? i * gridSize + j : (i + 1) * gridSize + j;
                int c = (j == 0) ? i * gridSize + j : i * gridSize + j - 1;
                int d = (j == gridSize - 1) ? i * gridSize + j : i * gridSize + j + 1;

                v[n] = (u[a] + u[b] + u[c] + u[d]) / 4.0f - u[n];
                v[n] *= 0.99f;
                u[n] += v[i * gridSize + j];
                vertices[n].y = u[n];
            }
        }
    }
    */

    /*
    private int floatToInt(float f)
    {
        f += 1;
        f *= 65536 / 2;
        int i = (int)f;
        return i;
    }

    private float intToFloat(int i)
    {
        float f = (float)i;
        f /= (65536 / 2);
        f = f - 1;
        return f;
    }
    */

}

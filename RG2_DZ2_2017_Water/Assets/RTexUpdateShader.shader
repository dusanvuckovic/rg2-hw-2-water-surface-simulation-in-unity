﻿Shader "Unlit/RTexUpdateShader"
{
	Properties {
		_Tex("InputTex", 2D) = "white" {}
		_GridSize("GridSize", Int) = 256
		_WaveSpeed("Wave speed", Range(0.0, 1.0)) = 0.5	
	}
	SubShader {
		Pass {
		Name "UpdateTex"
		CGPROGRAM
		#include "UnityCustomRenderTexture.cginc"

		#pragma vertex CustomRenderTextureVertexShader
		#pragma fragment frag
		#pragma target 3.0
		
		sampler2D _Tex;
		int _GridSize;
		float _WaveSpeed;

		float4 frag(v2f_customrendertexture IN) : COLOR {
			float4 start = tex2D(_Tex, IN.localTexcoord.xy);

			int x = IN.localTexcoord.x;
			int y = IN.localTexcoord.y;

			int coordLeft = max(0, x - 1);
			int coordRight = min(_GridSize, x + 1);
			int coordDown = max(0, y-1);
			int coordUp = min(_GridSize, y+1);

			int2 xL = int2(coordLeft, y);
			int2 xR = int2(coordRight, y);
			int2 yD = int2(x, coordDown);
			int2 yU = int2(x, coordUp);

			float4 left = tex2D(_Tex, xL);
			float4 right = tex2D(_Tex, xR);
			float4 down = tex2D(_Tex, yD);
			float4 up = tex2D(_Tex, yU);

			float newV = (left.r + right.r + down.r + up.r)/4.0f - start.r;
			newV *= _WaveSpeed/60;
			float newU = start.r + newV;

			//return float4(0, input, 0, 1);
			return float4(newU, newV, 0, 1);
			//return start;

			//float4 left = tex2D(_Tex, int2(IN.localTexcoord.x))

			/*int n = i * gridSize + j;
			int a = (i == 0) ? i * gridSize + j : (i - 1) * gridSize + j;
			int b = (i == gridSize - 1) ? i * gridSize + j : (i + 1) * gridSize + j;
			int c = (j == 0) ? i * gridSize + j : i * gridSize + j - 1;
			int d = (j == gridSize - 1) ? i * gridSize + j : i * gridSize + j + 1;

			v[n] = (u[a] + u[b] + u[c] + u[d]) / 4.0f - u[n];
			v[n] *= 0.99f;
			u[n] += v[i * gridSize + j];
			vertices[n].y = u[nbg];*/


			//return float4(max(0, start.r-0.001f), start.g, start.b, 1);
			//return float4(1, 0, 0, 1);//
		}
		ENDCG
	}
	}
}
